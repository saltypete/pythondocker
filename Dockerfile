FROM centos:7

RUN yum -y install epel-release && \
    yum -y install centos-release-scl-rh && \
    yum -y install devtoolset-8 && \
    source /opt/rh/devtoolset-8/enable && \
    yum -y install xerces-c-devel xeyes && \
    gcc --version

RUN source /opt/rh/devtoolset-8/enable && \
    yum -y install wget && \
    yum -y install cmake3

ENV HEP_OSLIBS_VER 7.2.5-1.el7.cern
RUN source /opt/rh/devtoolset-8/enable && \
    yum -y install epel-release && \
    yum -y install http://linuxsoft.cern.ch/wlcg/centos7/x86_64/HEP_OSlibs-${HEP_OSLIBS_VER}.x86_64.rpm && \
    yum -y update && \
    rpm -qa > /var/lib/package-list && \
    rm -fr /var/lib/yum/* /var/cache/yum/* /usr/share/doc/* /boot/*


ENV ROOT_VER v6.24.00
RUN source /opt/rh/devtoolset-8/enable && \
    cd /opt/ &&\
    wget -nv https://root.cern/download/root_${ROOT_VER}.Linux-centos7-x86_64-gcc4.8.tar.gz && \
    tar xzf root_${ROOT_VER}.Linux-centos7-x86_64-gcc4.8.tar.gz && \
    source /opt/root/bin/thisroot.sh

COPY run.sh /

RUN chmod a+x /run.sh
RUN mkdir /working

RUN pip3 install --upgrade pip && \
	pip3 install numpy && \
	pip3 install numba && \
	pip3 install scipy && \
	pip3 install uproot && \
	pip3 install colorama && \
	pip3 install tqdm  && \
	pip3 install matplotlib  && \
	pip3 install black && \
    pip3 install coloredlogs

ENTRYPOINT ["/run.sh"]
CMD ["echo No command was given"]
